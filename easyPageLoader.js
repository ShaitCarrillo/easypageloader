class epl {
    constructor(configuration){
        if(!configuration) configuration = {}
        this.filesPath = (configuration.filesPath)? configuration.filesPath : ''
        this.container = document.getElementById( (configuration.selector)? configuration.selector : 'container_area' )
    }

    init_elements(configuration){
        if(!configuration) configuration = {}
        for(let element of document.getElementsByClassName((configuration.selector)? configuration.selector : 'epl_a')){
            let target = element.getAttribute('link-target')
            element.addEventListener('click' , () => {
                this.getHTMLpage( (this.filesPath != '')? `${this.filesPath}${target}.html` : `${target}.html` , 
                    function(responseText, container, error) {
                        container.innerHTML = responseText
                        if(configuration.callback && !error)
                            if(configuration.callback[target]) configuration.callback[target]()
                    }
                )
            })
        }
    }

    getHTMLpage(file_dir, callback){
        let container = this.container 
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() { 
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                callback(xmlHttp.responseText, container, null);
            else if(xmlHttp.status == 404)
                callback(`Error file: ${file_dir} not found`, container, 'not_found')
        }
        xmlHttp.open("GET", file_dir, true);
        xmlHttp.send(null);
    }
}