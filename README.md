# EasyPageLoader

This tiny library allows you to load html files into your main html without loading a full new page

## Usage

### HTML makeup

First of all we must to create some elements that will trigger the page load, in the example we'll use a simple list with a tags:

``` html
 <ul>
    <li>
       <a class="epl_a" link-target="one">to one</a>
    </li>
    <li>
        <a class="epl_a" link-target="two">to two</a>
    </li>
    <li>
        <a class="epl_a" link-target="three">to three</a>
    </li>
</ul>
```

And don't forget to import the scripts!
``` HTML
<script src="easyPageLoader.js"></script>
<script src="main.js"></script>
```

As you can see all a tags have **_epl_a_** class, this one is used by the library to recognize them as loader's trigger.  
The attribute **_link-target_** is used to know which html partial file is going to be loaded. There is no need of add ".html" extension.
To define where you want to display de loaded content, set a parent element with id = "container_area":  
``` HTML
<div id="container_area"></div>
```

### Javascript usage

Simply create a new object of EasyPageLoader as shown below:

``` javascript
Const loader = new epl();
```

By default EPL uses root directory and container id = "container_area", if you want to use other directory or id, just send it in the constructor:  

``` javascript
const loader = new epl({filesPath : 'myDirectory/' , selector : 'mySelector'})
```

> filesPath and selector, are both optional

And then call to _init_elements()_ method:

``` javascript
loader.init_elements()
```
That's it!. Now you have a web page that doesn't load a whole HTML file to change of page.

#### Javascript for loaded partial HTML files

Here you have two options:  

Write all javascript in each partial HTML file(not recommended)... Or send a callback's JSON to _init_elements()_.  
Also, you can customize the class to use your own instead **_epl_a_**.  

``` javascript
const callbacks = {
    one : ()=>{
        alert('loaded one')
    },
    two : ()=>{
        alert('loaded a two')
    }
}

loader.init_elements({callback : functions, selector : 'my_selector'})
```

> Callback and selector, are both optional